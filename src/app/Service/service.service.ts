
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Empresa } from '../Model/Empresa';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  

  constructor(private http:HttpClient) { }

  Url='http://localhost:8080/otima/empresas';


  getEmpresas(){
    return this.http.get<Empresa[]>(this.Url);
  }

  createdEmpresa(empresa:Empresa){
    return this.http.post<Empresa>(this.Url, empresa);
  }

  getEmpresaId(id:number){

    console.log(+id);
    
    return this.http.get<Empresa>(this.Url+"/"+id);
    
  }
  updateEmpresa(empresa:Empresa){
    return this.http.put<Empresa>(this.Url+"/"+empresa.id, empresa);
  }

  deleteEmpresa(empresa:Empresa){
    return this.http.delete<Empresa>(this.Url+"/"+empresa.id);
    
  }
}
