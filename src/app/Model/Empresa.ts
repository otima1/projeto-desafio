
export class Empresa{
    id: number;
    cnpj: String;
    nome: String;
    tipo: String;
    razaoSocial: String;
    contato: String;
    email: String;
    cep: String;
    estado: String;
    bairro: String;
    cidade: String;
    logradouro: String;
    complemento: String;
    
}
