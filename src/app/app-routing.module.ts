import { EditComponent } from './empresa/edit/edit.component';
import { NovoComponent } from './empresa/novo/novo.component';

import { ListarComponent } from './empresa/listar/listar.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';





const routes: Routes = [
  {path: 'listar', component: ListarComponent},
  {path: 'novo', component: NovoComponent},
  {path: 'edit', component: EditComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
