import { HttpClient } from '@angular/common/http';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router';
import { Empresa } from 'src/app/Model/Empresa';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  empresa:Empresa = new Empresa();

  constructor(private router:Router, private service:ServiceService, private http:HttpClient) { }

  ngOnInit(): void {
    this.Editar();
  }

  Editar(){
    let id=localStorage.getItem("id");
    
    this.service.getEmpresaId(+id)
    .subscribe(data =>{
      this.empresa=data;
    })
  }

  consultaCep(cep, form) {
    cep = cep.replace(/\D/g, '');
    if (cep != "") {
      var validacep = /^[0-9]{8}$/;
      if (validacep.test(cep)) {
        this.http.get(`//viacep.com.br/ws/${cep}/json`).subscribe(dados => this.populaDadosForm(dados, form));
      }
    }
  }

  populaDadosForm(dados, f) {
    f.form.patchValue({
      bairro: dados.bairro,
      cep: dados.cep,
      cidade: dados.localidade,
      complemento: dados.complemento,
      estado: dados.uf,
      logradouro: dados.logradouro,
    });
  }

  Update(empresa){
    this.service.updateEmpresa(empresa)
    .subscribe(data =>{
      this.empresa=data;
      alert("Empresa Atualizada com sucesso!")
      this.router.navigate(["listar"]);
    })
  }

}
