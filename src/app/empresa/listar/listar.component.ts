import { Empresa } from '../../Model/Empresa';
import { ServiceService } from './../../Service/service.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  nome: String;
  cnpj:String;
  empresa: Empresa = new Empresa();

  empresas: Empresa[];

  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.service.getEmpresas()
      .subscribe(data => {
        this.empresas = data;
      })
      this.router.navigate(['listar']);
  }

  Editar(empresa: Empresa) {
    localStorage.setItem("id", empresa.id.toString());
    this.router.navigate(['edit']);
  }

  Delete(empresa: Empresa) {

    var resposta = confirm("Deseja remover esse registro?");
     if (resposta == true) {
      this.empresas = this.empresas.filter(p => p !== empresa);
   
      this.service.deleteEmpresa(empresa)
        .subscribe(data => {
          alert("Empresa deletada com sucesso!");
     });
     this.router.navigate(['listar']);
     }
     
  }
  
  Novo(){
    this.router.navigate(["novo"]);
  }
  Listar(){
    this.router.navigate(['listar']);
  }



}
