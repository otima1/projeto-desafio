import { Empresa } from 'src/app/Model/Empresa';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-novo',
  templateUrl: './novo.component.html',
  styleUrls: ['./novo.component.css']
})
export class NovoComponent implements OnInit {

  empresa:Empresa = new Empresa();

  constructor(private router: Router, private servica: ServiceService, private http: HttpClient) { }
 
  

  ngOnInit(): void {
  }

  consultaCep(cep, form) {
    cep = cep.replace(/\D/g, '');
    if (cep != "") {
      var validacep = /^[0-9]{8}$/;
      if (validacep.test(cep)) {
        this.http.get(`//viacep.com.br/ws/${cep}/json`).subscribe(dados => this.populaDadosForm(dados, form));
      }
    }
  }

  populaDadosForm(dados, f) {
    f.form.patchValue({
      bairro: dados.bairro,
      cep: dados.cep,
      cidade: dados.localidade,
      complemento: dados.complemento,
      estado: dados.uf,
      logradouro: dados.logradouro,
    });
  }


  Salvar(empresa: Empresa){
    this.servica.createdEmpresa(this.empresa)
    .subscribe(data =>{
      alert("Empresa cadastrada com sucesso!");
     this.router.navigate(["listar"]);
    })
  }
 
    
  
}
